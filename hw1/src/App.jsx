import React from 'react';
import Button from './component/Button/Button.js';
import Modal from './component/Modal/Modal.js';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstModalOpen: false,
      secondModalOpen: false,
    };
  }

  toggleFirstModal = () => {
    this.setState((prevState) => ({
      firstModalOpen: !prevState.firstModalOpen
    }));
  };

  toggleSecondModal = () => {
    this.setState({ secondModalOpen: !this.state.secondModalOpen });
  };

  render() {
    const firstModalActions = (
      <>
        <Button
          backgroundColor="#FF5733"
          text="Cancel"
          onClick={this.toggleFirstModal}
        />
        <Button
          backgroundColor="#5CB85C"
          text="Submit"
          onClick={this.toggleFirstModal}
        />
      </>
    );

    const secondModalActions = (
      <>
        <Button
          backgroundColor="#FF5733"
          text="Close"
          onClick={this.toggleSecondModal}
        />
      </>
    );

    return (
      <div className="App">
        <Button
          backgroundColor="#007BFF"
          text="Open first modal"
          onClick={this.toggleFirstModal}
        />
        <Button
          backgroundColor="#FFC107"
          text="Open second modal"
          onClick={this.toggleSecondModal}
        />

        {this.state.firstModalOpen && (
          <Modal
            header="First Modal"
            closeButton={true}
            text="This is the content of the first modal."
            actions={firstModalActions}
            onClose={this.toggleFirstModal}
          />
        )}

        {this.state.secondModalOpen && (
          <Modal
            header="Second Modal"
            closeButton={true}
            text="This is the content of the second modal."
            actions={secondModalActions}
            onClose={this.toggleSecondModal}
          />
        )}
      </div>
    );
  }
}

export default App;

